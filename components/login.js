import React, {Component} from 'react';
import {
  Container,
  Header,
  Content,
  Item,
  Input,
  Body,
  Button,
  Left,
  Icon,
  Title,
  Right,
  ListItem,
  CheckBox,
  List,
} from 'native-base';
import {StyleSheet, View, Text, AsyncStorage, Alert} from 'react-native';
import scenario from '../assets/scenario.json';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      checked: false,
      scenarioJson: scenario,
    };
  }
  componentDidMount() {
    const {scenarioJson} = this.state;
  }
  eventChange = (index) => {
    const {scenarioJson} = this.state;
    scenarioJson[index].checked=!scenarioJson[index].checked
    // scenarioJson[index].checked = [undefined, false].includes(scenarioJson[index].checked)
    this.setState({scenarioJson});

  };
  home = () => {
    this.props.navigation.navigate('homepage');
  };
  profile = () => {
    // console.log('----------data----------', data.email, this.state.email);

    this.props.navigation.navigate('profile');
  };
  render() {
    const {email, password, checked} = this.state;
    return (
      <Container>
      {/* <Content> */}
        <Header>
          <Left style={{flex: 1}}>
            <Button onPress={this.home} transparent>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body style={{flex: 2, alignItems: 'center'}}>
            <Text style={styles.header}>LOGIN</Text>
          </Body>
          <Right style={{flex: 1}} />
        </Header>
        <View style={styles.Space}>
          <Item style={styles.profile} rounded>
            <Input
              value={email}
              onChangeText={text => this.setState({email: text})}
              placeholder="Email"
            />
          </Item>
          <Item style={styles.profile} rounded>
            <Input
              value={password}
              onChangeText={text => this.setState({password: text})}
              secureTextEntry={true}
              placeholder="Password"
            />
          </Item>
          {/* <List>
          {this.state.scenarioJson.map((response, index) => {
            console.log('-------------eventmap----------', response)
          return (
            <ListItem>
            <CheckBox onPress={e => this.eventChange(index)} checked={response.checked} />
            <Text>Daily Stand Up</Text>
          </ListItem>
          )
          })}
          </List> */}
         
        </View>
        <View>
          <Button onPress={this.profile} style={styles.button} rounded light>
            <Text>LOGIN</Text>
          </Button>
        </View>
        {/* </Content> */}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  Space: {
    marginTop: 180,
    height: '20%',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  profile: {
    height: 50,
    width: '80%',
  },
  header: {
    color: 'white',
    fontSize: 25,
    textAlign: 'center',
  },
  button: {
    width: '80%',
    justifyContent: 'center',
    alignSelf: 'center',
  },
});
