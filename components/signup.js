/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {View, StyleSheet, AsyncStorage} from 'react-native';
import Moment from 'moment';
import {
  Container,
  Header,
  Content,
  Item,
  DatePicker,
  Input,
  Icon,
  Button,
  Text,
  Left,
  Body,
  Right,
} from 'native-base';

class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userDetail: {
        name: '',
        email: '',
        password: '',
        resetPassword: '',
        dob: '',
      },
      showPassword: false,
    };
  }
  user = () => {
    this.props.navigation.navigate('profile', {user: this.state.userDetail});
    AsyncStorage.setItem('user', JSON.stringify(this.state.userDetail));
  };
  home = () => {
    this.props.navigation.navigate('homepage');
  };

  setValue = (text, key) => {
    let {userDetail} = this.state;

    if (key === 'dob') {
      console.log('-------------1--------------');
      userDetail[key] = Moment(text).format('DD/MM/YYYY');
    }
    console.log('----------------2----------');
    userDetail[key] = text;

    this.setState({userDetail});
  };

  render() {
    const {
      name,
      email,
      password,
      resetPassword,
      userDetail,
      dob,
      showPassword,
    } = this.state;
    return (
      <Container>
        <Header>
          <Left style={{flex: 1}}>
            <Button onPress={this.home} transparent>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body style={{flex: 2, alignItems: 'center'}}>
            <Text style={styles.header}>SIGN UP</Text>
          </Body>
          <Right style={{flex: 1}} />
        </Header>
        <View style={styles.Space}>
          <Item style={styles.child}>
            <Input
              placeholder="Your Name"
              value={this.state.name}
              onChangeText={text => this.setValue(text, 'name')}
            />
            <Icon active name="person" />
          </Item>
          <Item style={styles.child}>
            <Input
              placeholder="Your Email"
              value={this.state.email}
              onChangeText={text => this.setValue(text, 'email')}
            />
            <Icon active name="mail" />
          </Item>
          <Item style={styles.child}>
            <Input
              secureTextEntry={!showPassword}
              placeholder="Password"
              value={this.state.password}
              onChangeText={text => this.setValue(text, 'password')}
            />
          </Item>
          <Item style={styles.child}>
            <Input
              secureTextEntry={!showPassword}
              value={this.state.resetPassword}
              onChangeText={text => this.setValue(text, 'resetPassword')}
              placeholder="Repeat your password"
            />
            <Icon
              onPress={() => this.setState({showPassword: !showPassword})}
              active
              name={showPassword ? 'eye' : 'eye-off'}
            />
          </Item>
          <DatePicker
            defaultDate={new Date()}
            locale={'en'}
            timeZoneOffsetInMinutes={undefined}
            modalTransparent={false}
            animationType={'fade'}
            androidMode={'default'}
            placeHolderText="Date of birth"
            placeHolderTextStyle={{color: 'grey'}}
            onDateChange={text => this.setValue(text, 'dob')}
            disabled={false}
          />
        </View>
        <View style={styles.button}>
          <Button
            style={{justifyContent: 'center'}}
            rounded
            light
            onPress={this.user}>
            <Text>SIGN UP</Text>
          </Button>
        </View>
      </Container>
      //   <View>
      //     <Text>This is the home screen</Text>
      //     <Button onPress={() => this.props.navigation.navigate('google')} title="Google"/>
      //     <Button onPress={() => this.props.navigation.navigate('user')} title="User"/>

      //   </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    color: 'white',
    fontSize: 25,
    textAlign: 'center',
  },
  Space: {
    marginTop: 60,
    alignItems: 'center',
  },
  child: {
    height: 40,
    width: '60%',
    margin: 30,
    borderColor: 'black',
  },
  button: {
    width: '50%',
    justifyContent: 'center',
    alignSelf: 'center',
  },
});

export default Signup;
