import React, {Component} from 'react';
import {
  Container,
  Header,
  Content,
  Button,
  Text,
  Body,
  Left,
  Right,
} from 'native-base';
import {StyleSheet, View, Image, Dimensions} from 'react-native';
import scenario from '../assets/scenario.json';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    let scenarioJson = scenario;
   
  }
  componentWillMount() {}
  componentDidUpdate() {}
  componentWillUnmount() {}
  componentWillReceiveProps() {}
  render() {
    const window = Dimensions.get('window');

    return (
      <Container>
        <Header>
          <Left style={{flex: 1}} />
          <Body style={{flex: 2, alignItems: 'center'}}>
            <Text style={styles.header}>TODO</Text>
          </Body>
          <Right style={{flex: 1}} />
        </Header>
        <View style={styles.Space}>
          <Image
            style={styles.backgroundImage}
            source={require('../assets/59eff9f5abd3555854075c3fc0fb5b63.jpg')}
          />
          <Button
            style={styles.button}
            info
            onPress={() => this.props.navigation.navigate('google')}>
            <Text> GOOGLE MAP </Text>
          </Button>
          <Button
            style={styles.button}
            info
            onPress={() => this.props.navigation.navigate('login')}>
            <Text> LOGIN </Text>
          </Button>
          <Button
            info
            style={styles.button}
            primaryStyleSheet
            onPress={() => this.props.navigation.navigate('signup')}>
            <Text> SIGN UP </Text>
          </Button>
          <Button
            info
            style={styles.button}
            primaryStyleSheet
            onPress={() => this.props.navigation.navigate('flatlist')}>
            <Text> FLATLIST </Text>
          </Button>
        </View>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'green',
  },
  Space: {
    marginTop: 120,
    height: '50%',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  button: {
    width: '60%',
    height: 50,
    borderRadius: 40,
    justifyContent: 'center',
  },
  header: {
    color: 'white',
    fontSize: 25,
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
    position: 'absolute',
    width: '100%',
    height: '100%',
    justifyContent: 'center',
  },
});

export default Home;
