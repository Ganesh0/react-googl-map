/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Label,
  Button,
  Icon,
  Left,
  Body,
  Text,
  Right,
  Thumbnail,
} from 'native-base';
import {StyleSheet, View, AsyncStorage} from 'react-native';

class Profile extends Component {
  constructor(props){
    super(props);
    this.state = {
      user: {}
     
    }
  }
  async componentDidMount() {
    console.log('-----------------------props', this.props)
    let user = await AsyncStorage.getItem('user');
    user = JSON.parse(user)
    this.setState({user})
    console.log('-----------------useer----------', user)
  }
  signup = () => {
    this.props.navigation.navigate('signup');
  };
  render() {
    const {user} = this.state
    return (
      <Container>
        <Header>
          <Left style={{flex: 1}}>
            <Button onPress={this.signup} transparent>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body style={{flex: 2, alignItems: 'center'}}>
            <Text style={styles.header}> USER PROFILE</Text>
          </Body>
          <Right style={{flex: 1}} />
        </Header>
        <View style={styles.thumb}>
          <Thumbnail large source={require('../assets/profile_user.jpg')} />
        </View>
        <View style={styles.Space}>
          <Form>
            <Item style={styles.profile} rounded>
              <Label style={styles.lab}>Username : </Label>
              <Input disabled value={user.name} />
            </Item>
            <Item style={styles.profile} rounded>
              <Label style={styles.lab}>Email: </Label>
              <Input disabled value={user.email} />
            </Item>
            <Item style={styles.profile} rounded>
              <Label style={styles.lab}>Password : </Label>
              <Input disabled value={user.password} />
            </Item>
            <Item style={styles.profile} rounded>
              <Label style={styles.lab}>Resetpassword : </Label>
              <Input
                disabled
                value={user.resetPassword}
              />
            </Item>
            <Item style={styles.profile} rounded>
              <Label style={styles.lab}>DateOfBirth : </Label>
              <Input disabled value={user.dob} />
            </Item>
          </Form>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  Space: {
    marginTop: 150,
    height: '30%',
    justifyContent: 'space-evenly',
    alignSelf: 'center',
    alignItems: 'center',
    width: '80%',
  },
  profile: {
    height: 60,
    margin: 20,
    width: '90%',
    paddingHorizontal: 15,
  },

  header: {
    color: 'white',
    fontSize: 25,
    textAlign: 'center',
  },
  button: {
    width: '50%',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  thumb: {
    alignItems: 'center',
    marginTop: 30,
  },
});

export default Profile;
