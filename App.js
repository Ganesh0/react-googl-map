import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import {StackNavigator} from 'react-navigation';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import {Ionicons} from 'react-native-vector-icons';
import {Icon} from 'native-base'

import Google from './components/google';
import Signup from './components/signup';
import Profile from './components/profile';
import HomePage from './components/home';
import Login from './components/login';
import Flatlist from './components/flatlist';

const Stack = createStackNavigator();
const Tab = createMaterialBottomTabNavigator();

export default class App extends Component {
  render() {
    return (
      <NavigationContainer>
        <Tab.Navigator
          screenOptions={({route}) => ({
            tabBarIcon: ({focused, color, size}) => {
              let iconName;

              if (route.name === 'homepage') {
                iconName = focused
                  ? 'information-circle'
                  : 'information-circle-outline';
              } else if (route.name === 'google') {
                iconName = focused ? 'list-box' : 'list';
              }

              // You can return any component that you like here!
              return <Icon name={iconName} size={size} color={color} />;
            },
          })}
          tabBarOptions={{
            activeTintColor: 'tomato',
            inactiveTintColor: 'gray',
          }}>
          <Tab.Screen name="google" component={Google} />
          <Tab.Screen name="homepage" component={HomePage} />
        </Tab.Navigator>
      </NavigationContainer>
    );
  }
}
